var editor = new JSONEditor(document.getElementById('editor_holder'), {
    disable_collapse: true,
    disable_edit_json: true,
    disable_properties: true,
    schema: {
        "readOnly": false,
        "$schema": "http://json-schema.org/draft-04/hyper-schema",
        "description": "",
        "type": "object",
        "properties": {
            "financialEmployment": {
                "type": "object",
                "title": "Financial & Employment information",
                "properties": {
                    "ddlEmploymentStatus": {
                        "id": "ddlEmploymentStatus",
                        "type": "number",
                        "title": "Please provide details about your employment status",
                        "enum": [0, 1, 2, 3, 4, 5],
                        "options": {
                            "enum_titles": ["Select option", "Salaried Employee", "Retired", "Self Employed", "Student", "Unemployed"]
                        }
                    },
                    "CheckBoxListIndustry": {
                        "id": "CheckBoxListIndustry",
                        "title": "Industry",
                        "format": "checkbox",
                        "type": "array",
                        "uniqueItems": true,
                        "items": {
                            "type": "number",
                            "enum": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                            "options": {
                                "enum_titles": [
                                    "Financial Services, Banking, Accountancy",
                                    "Business Consulting, Management, marketing",
                                    "Hospitality & Tourism & Leisure",
                                    "Health Care",
                                    "Engineering & Manufacturing",
                                    "Environment and Agriculture",
                                    "Energy and Utilities",
                                    "Public Services and administration",
                                    "Property and Construction",
                                    "Retail and sales",
                                    "I.T., online media and Marketing, creative arts and design",
                                    "Science Research and Pharmaceuticals",
                                    "Transport and logistics",
                                    "Teaching and Education",
                                    "Other"
                                ]
                            }
                        }
                    },
                    "CheckBoxListSourceOfFunds": {
                        "id": "CheckBoxListSourceOfFunds",
                        "title": "Source of Funds",
                        "format": "checkbox",
                        "type": "array",
                        "uniqueItems": true,
                        "items": {
                            "type": "number",
                            "enum": [1, 2, 3, 4, 5, 6],
                            "options": {
                                "enum_titles": [
                                    "Saving",
                                    "Private Pension",
                                    "Partner/Family/Friend",
                                    "Investments",
                                    "Benefits/Borrowing",
                                    "Other"
                                ]
                            }
                        }
                    },
                }
            },
            "knowledgeExperience": {
                "type": "object",
                "title": "Knowledge & Experience",
                "properties": {
                    "ddlLevelOfEducation": {
                        "type": "number",
                        "title": "What is your level of education?",
                        "enum": [0, 1, 2, 3, 4, 5],
                        "options": {
                            "enum_titles": ["Select option", "High School or Equivalent", "University", "Self Employed", "Post-Graduate", "None"]
                        }
                    },
                    "understand_fxcfd_price_differences": {
                        "type": "number",
                        "title": "Do you understand that the price of FX/CFDs are derived from the prices of their underlying assets but are not the same?",
                        "enum": [0, 1, 2],
                        "options": {
                            "enum_titles": ["Select option", "Yes", "No"]
                        }
                    },
                    "understand_otc_transaction": {
                        "type": "number",
                        "title": "Do you understand that FX/CFDs are executed Over-the-Counter (OTC) and not via an exchange?",
                        "enum": [0, 1, 2],
                        "options": {
                            "enum_titles": ["Select option", "Yes", "No"]
                        }
                    },
                    "understand_leveraged_loss": {
                        "type": "number",
                        "title": "Do you understand that when trading leveraged products such as Forex/CFDs it is possible to lose more than your invested capital?",
                        "enum": [0, 1, 2],
                        "options": {
                            "enum_titles": ["Select option", "Yes", "No"]
                        }
                    },
                    "last_year_trading_course": {
                        "type": "number",
                        "title": "Have you attended a seminar or course related to Financial Markets / Investments / Online Trading during the last year?",
                        "enum": [0, 1, 2],
                        "options": {
                            "enum_titles": ["Select option", "Yes", "No"]
                        }
                    },
                    "trading_kowledge": {
                        "type": "number",
                        "title": "Have you used in the past non-advised investments services that require independent decisions?",
                        "enum": [0, 1, 2],
                        "options": {
                            "enum_titles": ["Select option", "Yes", "No"]
                        }
                    },
                    "financialInstruments": {
                        "id": "financialInstruments",
                        "title": "Please select all of the financial instruments that you have traded in the past?",
                        "format": "checkbox",
                        "type": "array",
                        "uniqueItems": true,
                        "items": {
                            "type": "number",
                            "enum": [1, 2, 3, 4, 5],
                            "options": {
                                "enum_titles": [
                                    "Bonds, Stocks / Shares, Futures, etc.",
                                    "Binary Options",
                                    "Forex/CFDs",
                                    "Other derivative products",
                                    "None"
                                ]
                            }
                        }
                    },
                    "financialInstrumentsList": {
                        "id": "financialInstrumentsList",
                        "title": " ",
                        "type": "object",
                        "properties": {
                            "finInstruments_trading_experience": {
                                "type": "number",
                                "title": "How long have you been investing in the financial markets?",
                                "enum": [0, 1, 2, 3, 4],
                                "options": {
                                    "enum_titles": [
                                        "Select option", 
                                        "0 – 6 months", 
                                        "6 – 12 months",
                                        "1 – 5 years",
                                        "Over 5 years",
                                    ]
                                }
                            },
                            "finInstruments_last_transactions_carried_out": {
                                "type": "number",
                                "title": "When was your last trade carried out?",
                                "enum": [0, 1, 2, 3, 4],
                                "options": {
                                    "enum_titles": [
                                        "Select option", 
                                        "Less than 1 month ago", 
                                        "1 - 6 months",
                                        "6 - 12 months",
                                        "More than 12 months ago"
                                    ]
                                }
                            },
                            "finInstruments_average_annual_volume": {
                                "type": "number",
                                "title": "What was the total volume of your past transactions during this period?",
                                "enum": [0, 1, 2, 3, 4],
                                "options": {
                                    "enum_titles": [
                                        "Select option", 
                                        "Less than $ 100,000", 
                                        "$100,000 - $ 500,000",
                                        "$500,000 - $ 1,500,000",
                                        "Above $ 1,500,000"
                                    ]
                                }
                            },
                            "finInstruments_maximum_leverage_used": {
                                "type": "number",
                                "title": "What is the maximum leverage you used?",
                                "enum": [0, 1, 2, 3, 4, 5],
                                "options": {
                                    "enum_titles": [
                                        "Select option",
                                        "1:50",
                                        "1:100",
                                        "1:200",
                                        "1:400",
                                        "1:500"
                                    ]
                                }
                            },
                            "finInstruments_education_professional_qualifications": {
                                "type": "number",
                                "title": "Do you have prior education, professional qualifications related to the services offered?",
                                "enum": [0, 1, 2],
                                "options": {
                                    "enum_titles": [
                                        "Select option",
                                        "Yes",
                                        "No"
                                    ]
                                }
                            },
                            "finInstruments_services_prior_experience": {
                                "type": "number",
                                "title": "Do you have prior work experience related to the services offered?",
                                "enum": [0, 1, 2],
                                "options": {
                                    "enum_titles": [
                                        "Select option",
                                        "Yes",
                                        "No"
                                    ]
                                }
                            },
                            "ddlUsedLeveragedPoductsYesNo": {
                                "type": "number",
                                "title": "Have you used leveraged products, such as Margin Trading in Forex, CFDs, before?",
                                "enum": [0, 1, 2],
                                "options": {
                                    "enum_titles": [
                                        "Select option",
                                        "Yes",
                                        "No"
                                    ]
                                }
                            },
                            "ddlHaveExperienceFinMarket": {
                                "type": "number",
                                "title": "Have you experienced in the financials markets before? (including none-advised services)",
                                "enum": [0, 1, 2],
                                "options": {
                                    "enum_titles": [
                                        "Select option",
                                        "Yes",
                                        "No"
                                    ]
                                }
                            }
                        }
                    }
                }
            },
            "economicProfile": {
                "type": "object",
                "title": "Economic Profile",
                "properties": {
                    "ddlEstimatedYearlyIncome": {
                        "type": "number",
                        "title": "What is your estimated yearly income?",
                        "enum": [0, 1, 2, 3, 4],
                        "options": {
                            "enum_titles": [
                                "Select option",
                                "Less than $25,000",
                                "$25,000 – $100,000",
                                "$100,000 – $250,000",
                                "Above $250,000"

                            ]
                        }
                    },
                    "ddlEstimatedNetWorth": {
                        "type": "number",
                        "title": "What is your estimated net worth?",
                        "enum": [0, 1, 2, 3, 4],
                        "options": {
                            "enum_titles": [
                                "Select option",
                                "Less than $100,000",
                                "$100,000 – $500,000",
                                "$500,000 - $1,000,000",
                                "Above $1,000,000",
                            ]
                        }
                    },
                    "ddlAnticipatedAccountTurnover": {
                        "type": "number",
                        "title": "Anticipated Account Turnover",
                        "enum": [0, 1, 2, 3, 4],
                        "options": {
                            "enum_titles": [
                                "Select option",
                                "Less than $100,000",
                                "$100,000 – $500,000",
                                "$500,000 - $1,000,000",
                                "Above $1,000,000",
                            ]
                        }
                    },
                    "ddlUSReportablePersonYesNo": {
                        "type": "number",
                        "title": "Are you a U.S. reportable person?",
                        "enum": [0, 1, 2],
                        "options": {
                            "enum_titles": [
                                "Select option",
                                "Yes",
                                "No"
                            ]
                        }
                    },
                    "americanIndicate": {
                        "id": "americanIndicate",
                        "type": "string",
                        "title": "Please indicate your US TIN / GIIN number:"
                    },
                    "ddlTaxResidenceCountry": {
                        "type": "number",
                        "title": "What is your country of tax residence? ",
                        "enum": [0, 1, 2],
                        "options": {
                            "enum_titles": [
                                "Select option",
                                "USA",
                                "Agentina"
                            ]
                        }
                    },
                    "tinNumber": {
                        "type": "string",
                        "title": "Please state your TIN Number in this country:"
                    },
                    "risk": {
                        "type": "boolean",
                        "title": "I understand the risk and wish to proceed",
                        "format": "checkbox"
                    }
                }
            }

        }
    }
});

const buttonSubmit = document.getElementById('submit');
buttonSubmit.disabled = true;

buttonSubmit.addEventListener('click', function () {
    console.log(editor.getValue());
});

function getIdElement(title) {
    return document.querySelectorAll(`[data-schemaid=${title}]`)[0];
}

const checkBoxListIndustry = getIdElement('CheckBoxListIndustry');
checkBoxListIndustry.style.display = 'none';
const checkBoxListSourceOfFunds = getIdElement('CheckBoxListSourceOfFunds');
checkBoxListSourceOfFunds.style.display = 'none';
const financialInstrumentsList = getIdElement('financialInstrumentsList');
financialInstrumentsList.style.display = 'none';
const americanIndicate = getIdElement('americanIndicate');
americanIndicate.style.display = 'none';


editor.on("change", function () {
    const value = editor.getValue();

    if (value.financialEmployment.ddlEmploymentStatus === 1 || value.financialEmployment.ddlEmploymentStatus === 3)
        checkBoxListIndustry.style.display = 'block'
    else
        checkBoxListIndustry.style.display = 'none';

    if (value.financialEmployment.ddlEmploymentStatus === 2 || value.financialEmployment.ddlEmploymentStatus === 4 || value.financialEmployment.ddlEmploymentStatus === 5)
        checkBoxListSourceOfFunds.style.display = 'block'
    else
        checkBoxListSourceOfFunds.style.display = 'none';

    if (value.knowledgeExperience.financialInstruments.length > 0 && value.knowledgeExperience.financialInstruments.indexOf(5) === -1)
        financialInstrumentsList.style.display = 'block';
    else 
        financialInstrumentsList.style.display = 'none';

    if (value.knowledgeExperience.financialInstruments.indexOf(5) !== -1) {
        const finInstr = editor.getEditor("root.knowledgeExperience.financialInstruments");
        finInstr.setValue([5]);
    }

    if (value.economicProfile.ddlUSReportablePersonYesNo === 1)
        americanIndicate.style.display = 'block'
    else
        americanIndicate.style.display = 'none';

    if (value.economicProfile.risk)
        buttonSubmit.disabled = false;
    else
        buttonSubmit.disabled = true;
});

// editor.watch('terms', () => {
//   console.log('graduate.terms.first');
// })